﻿using System;
using System.Collections.Generic;

namespace SATU
{
    public partial class Product
    {
        public class ProductPresentation{
            public int? Id { get; set; }
            public string Name { get; set; }
            public decimal price { get; set; }
            public int? CategoryId { get; set; }
            public Activity.ActivityPresentation Category { get; set; }
        }

        public Product()
        {
            InvoiceItem = new HashSet<InvoiceItem>();
            ProductLatest = new HashSet<ProductLatest>();
        }

        public Product(ProductPresentation from) : this(){
            Dbid = null;
            Appid = from.Id;
            Name = from.Name;
            Price = from.price;
            if(from.CategoryId.HasValue){
                CategoryId = (int)from.CategoryId;
            }else{
                throw new System.ArgumentException("CategoryId cannot be null");
            }
        }

        public int? Dbid { get; set; }
        public int? Appid { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int CategoryId { get; set; }
        public ProductPresentation Presentation{ get{
            var r = new ProductPresentation();
            r.Id = Appid;
            r.Name = Name;
            r.price = Price;
            r.CategoryId = CategoryId;
            r.Category = CategoryNavigation.Presentation;
            return r;
        }}

        public virtual Activity CategoryNavigation { get; set; }
        public virtual ICollection<InvoiceItem> InvoiceItem { get; set; }
        public virtual ICollection<ProductLatest> ProductLatest { get; set; }
    }
}
