﻿using System;
using System.Collections.Generic;

namespace SATU
{
    public partial class Person
    {
        public class PersonPresentation{
            public string Id { get; set; }
            public string IdType { get; set; }
            public string Name { get; set; }
            public string Phone { get; set; }
            public string Email { get; set; }
            public string PersonType { get; set; }
            public int ActivityId { get; set; }
            public Activity.ActivityPresentation Activity{ get; set; }
        }
        public Person()
        {
            InvoiceIssueeNavigation = new HashSet<Invoice>();
            InvoiceIssuerNavigation = new HashSet<Invoice>();
            PersonLatest = new HashSet<PersonLatest>();
        }

        public Person(PersonPresentation from) : this(){
            Dbid = null;
            Id = from.Id;
            IdType = from.IdType;
            Name = from.Name;
            Phone = from.Phone;
            Email = from.Email;
            PersonType = from.PersonType;
            ActivityId = from.ActivityId;
        }

        public int? Dbid { get; set; }
        public string Id { get; set; }
        public string IdType { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string PersonType { get; set; }
        public int ActivityId { get; set; }
        public PersonPresentation Presentation  {get{
            return new PersonPresentation{
                Id = Id,
                IdType = IdType,
                Name = Name,
                Phone = Phone,
                Email = Email,
                PersonType = PersonType,
                ActivityId = ActivityId,
                Activity = ActivityNavigation.Presentation
            };
        }}

        public virtual Activity ActivityNavigation { get; set; }
        public virtual ICollection<Invoice> InvoiceIssueeNavigation { get; set; }
        public virtual ICollection<Invoice> InvoiceIssuerNavigation { get; set; }
        public virtual ICollection<PersonLatest> PersonLatest { get; set; }
    }
}
