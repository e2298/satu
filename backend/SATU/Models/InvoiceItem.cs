﻿using System;
using System.Collections.Generic;

namespace SATU
{
    public partial class InvoiceItem
    {
        public class InvoiceItemPresentation{
            public int ProductId { get; set; }
            public Product.ProductPresentation Product { get; set; }
            public decimal? Quantity { get; set; }
            public decimal? Discount { get; set; }
        }

        public InvoiceItem(){

        }

        public InvoiceItem(InvoiceItemPresentation from, int InvoiceId, int productId){
            IdinvoiceItem = null;
            Product = productId;
            Quantity = from.Quantity ?? 1.0M;
            Discount = from.Discount ?? 0.0M;
            Invoice = InvoiceId;
        }

        public int? IdinvoiceItem { get; set; }
        public int Product { get; set; }
        public decimal? Quantity { get; set; }
        public decimal? Discount { get; set; }
        public int Invoice { get; set; }
        public InvoiceItemPresentation Presentation { get{
            return new InvoiceItemPresentation{
                ProductId = (int)ProductNavigation.Appid,
                Product = ProductNavigation.Presentation,
                Quantity = (decimal)Quantity,
                Discount = (decimal)Discount
            };
        }}

        public virtual Invoice InvoiceNavigation { get; set; }
        public virtual Product ProductNavigation { get; set; }
    }
}
