﻿using System;
using System.Collections.Generic;

namespace SATU
{
    public partial class ProductLatest
    {
        public int Appid { get; set; }
        public int? Latest { get; set; }

        public virtual Product LatestNavigation { get; set; }
    }
}
