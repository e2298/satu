﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.Configuration;

namespace SATU
{
    public partial class SATUContext : DbContext
    {
        public SATUContext()
        {
        }

        public SATUContext(DbContextOptions<SATUContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Activity> Activity { get; set; }
        public virtual DbSet<Invoice> Invoice { get; set; }
        public virtual DbSet<InvoiceItem> InvoiceItem { get; set; }
        public virtual DbSet<Person> Person { get; set; }
        public virtual DbSet<PersonLatest> PersonLatest { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<ProductLatest> ProductLatest { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql(
                    String.Format(
                        "Server={0};Database={1};User={2};Password={3}",
                        ConfigurationManager.AppSettings["DBHost"],
                        ConfigurationManager.AppSettings["DBName"],
                        ConfigurationManager.AppSettings["DBUser"],
                        ConfigurationManager.AppSettings["DBPassword"]
                    ),
                    x => x.ServerVersion("10.4.13-mariadb"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Activity>(entity =>
            {
                entity.HasKey(e => e.Dbid)
                    .HasName("PRIMARY");

                entity.ToTable("activity");

                entity.Property(e => e.Dbid)
                    .HasColumnName("DBID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Exists)
                    .HasColumnName("exists")
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(200)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_ci");

                entity.Property(e => e.Rate)
                    .HasColumnName("rate")
                    .HasColumnType("decimal(10,3)");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasColumnName("type")
                    .HasColumnType("enum('person','product')")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_ci");
            });

            modelBuilder.Entity<Invoice>(entity =>
            {
                entity.HasKey(e => e.Idinvoice)
                    .HasName("PRIMARY");

                entity.ToTable("invoice");

                entity.HasIndex(e => e.Issuee)
                    .HasName("issuee_index");

                entity.HasIndex(e => e.Issuer)
                    .HasName("issuer_index");

                entity.HasIndex(e => e.TimeIssued)
                    .HasName("time_index");

                entity.Property(e => e.Idinvoice)
                    .HasColumnName("idinvoice")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Currency)
                    .IsRequired()
                    .HasColumnName("currency")
                    .HasColumnType("enum('CRC','USD')")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_ci");

                entity.Property(e => e.Issuee)
                    .HasColumnName("issuee")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Issuer)
                    .HasColumnName("issuer")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PaymentType)
                    .IsRequired()
                    .HasColumnName("payment_type")
                    .HasColumnType("enum('card','cash','transfer')")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_ci");

                entity.Property(e => e.TimeIssued)
                    .HasColumnName("time_issued")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("'current_timestamp()'");

                entity.HasOne(d => d.IssueeNavigation)
                    .WithMany(p => p.InvoiceIssueeNavigation)
                    .HasForeignKey(d => d.Issuee)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("issuee_key");

                entity.HasOne(d => d.IssuerNavigation)
                    .WithMany(p => p.InvoiceIssuerNavigation)
                    .HasForeignKey(d => d.Issuer)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("issuer_key");
            });

            modelBuilder.Entity<InvoiceItem>(entity =>
            {
                entity.HasKey(e => e.IdinvoiceItem)
                    .HasName("PRIMARY");

                entity.ToTable("invoice_item");

                entity.HasIndex(e => e.Invoice)
                    .HasName("invoice_key_idx");

                entity.HasIndex(e => e.Product)
                    .HasName("product_key_idx");

                entity.Property(e => e.IdinvoiceItem)
                    .HasColumnName("idinvoice_item")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Discount)
                    .HasColumnName("discount")
                    .HasColumnType("decimal(10,3)")
                    .HasDefaultValueSql("'0.000'");

                entity.Property(e => e.Invoice)
                    .HasColumnName("invoice")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Product)
                    .HasColumnName("product")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Quantity)
                    .HasColumnName("quantity")
                    .HasColumnType("decimal(10,3)")
                    .HasDefaultValueSql("'1.000'");

                entity.HasOne(d => d.InvoiceNavigation)
                    .WithMany(p => p.InvoiceItem)
                    .HasForeignKey(d => d.Invoice)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("invoice_key");

                entity.HasOne(d => d.ProductNavigation)
                    .WithMany(p => p.InvoiceItem)
                    .HasForeignKey(d => d.Product)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("product_key");
            });

            modelBuilder.Entity<Person>(entity =>
            {
                entity.HasKey(e => e.Dbid)
                    .HasName("PRIMARY");

                entity.ToTable("person");

                entity.HasIndex(e => e.ActivityId)
                    .HasName("activity_idx");

                entity.HasIndex(e => e.Id)
                    .HasName("id");

                entity.Property(e => e.Dbid)
                    .HasColumnName("DBID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ActivityId)
                    .HasColumnName("activity")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varchar(256)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_ci");

                entity.Property(e => e.Id)
                    .IsRequired()
                    .HasColumnName("id")
                    .HasColumnType("varchar(30)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_ci");

                entity.Property(e => e.IdType)
                    .IsRequired()
                    .HasColumnName("id_type")
                    .HasColumnType("enum('natural','juridical','resident','passport')")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_ci");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_ci");

                entity.Property(e => e.PersonType)
                    .IsRequired()
                    .HasColumnName("person_type")
                    .HasColumnType("enum('recipient','issuer')")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_ci");

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasColumnName("phone")
                    .HasColumnType("varchar(20)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_ci");

                entity.HasOne(d => d.ActivityNavigation)
                    .WithMany(p => p.Person)
                    .HasForeignKey(d => d.ActivityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("activity");
            });

            modelBuilder.Entity<PersonLatest>(entity =>
            {
                entity.ToTable("person_latest");

                entity.HasIndex(e => e.Latest)
                    .HasName("fk_person_latest_1_idx");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("varchar(30)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_ci");

                entity.Property(e => e.Latest)
                    .HasColumnName("latest")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.LatestNavigation)
                    .WithMany(p => p.PersonLatest)
                    .HasForeignKey(d => d.Latest)
                    .HasConstraintName("fk_person_latest_1");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.HasKey(e => e.Dbid)
                    .HasName("PRIMARY");

                entity.ToTable("product");

                entity.HasIndex(e => e.CategoryId)
                    .HasName("category_idx");

                entity.Property(e => e.Dbid)
                    .HasColumnName("DBID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Appid)
                    .HasColumnName("APPID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CategoryId)
                    .HasColumnName("category")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(200)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_ci");

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("decimal(10,3)");

                entity.HasOne(d => d.CategoryNavigation)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("category");
            });

            modelBuilder.Entity<ProductLatest>(entity =>
            {
                entity.HasKey(e => e.Appid)
                    .HasName("PRIMARY");

                entity.ToTable("product_latest");

                entity.HasIndex(e => e.Latest)
                    .HasName("prod_idx");

                entity.Property(e => e.Appid)
                    .HasColumnName("APPID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Latest)
                    .HasColumnName("latest")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.LatestNavigation)
                    .WithMany(p => p.ProductLatest)
                    .HasForeignKey(d => d.Latest)
                    .HasConstraintName("product_latest_ibfk_1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
