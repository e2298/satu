﻿using System;
using System.Collections.Generic;

namespace SATU
{
    public partial class Activity
    {
        public class ActivityPresentation{
            public int Id { get; set; }
            public string Name { get; set; }
            public decimal Rate { get ;set; }
            public string Type { get; set; }
        }
        public Activity()
        {
            Person = new HashSet<Person>();
            Product = new HashSet<Product>();
        }

        public Activity(ActivityPresentation from) : this(){
            Dbid = from.Id;
            Name = from.Name;
            Rate = from.Rate;
            if(from.Type != "person" && from.Type != "product"){
                throw new ArgumentException("Type must be one of 'person' or 'product'");
            }
            Type = from.Type;
            Exists = 1;
        }

        public int Dbid { get; set; }
        public string Name { get; set; }
        public decimal Rate { get; set; }
        public string Type { get; set; }
        public sbyte Exists { get; set; }
        public ActivityPresentation Presentation { get{
            var r = new ActivityPresentation();
            r.Id = Dbid;
            r.Name = Name;
            r.Rate = Rate;
            r.Type = Type;
            return r;
        }}

        public virtual ICollection<Person> Person { get; set; }
        public virtual ICollection<Product> Product { get; set; }
    }
}
