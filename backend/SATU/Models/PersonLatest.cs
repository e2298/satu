﻿using System;
using System.Collections.Generic;

namespace SATU
{
    public partial class PersonLatest
    {
        public string Id { get; set; }
        public int? Latest { get; set; }

        public virtual Person LatestNavigation { get; set; }
    }
}
