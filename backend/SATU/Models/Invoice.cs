﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SATU
{
    public partial class Invoice
    {
        public class InvoicePresentation{
            public int? Id { get; set; }
            public DateTime Time { get; set; }
            public string IssuerId { get; set; }
            public Person.PersonPresentation Issuer { get; set; }
            public string RecipientId { get; set; }
            public Person.PersonPresentation Recipient { get; set; }
            public string PaymentType { get; set; }
            public string Currency { get; set; }
            public decimal? total { get; set; }
            public InvoiceItem.InvoiceItemPresentation[] Items { get; set; }

        }
        public Invoice()
        {
            InvoiceItem = new HashSet<InvoiceItem>();
        }
        public Invoice(InvoicePresentation from, int issuerId, int issueeId) : this(){
            Issuer = issuerId;
            Issuee = issueeId;
            PaymentType = from.PaymentType;
            Currency = from.Currency;
        }

        public int? Idinvoice { get; set; }
        public DateTime TimeIssued { get; set; }
        public int Issuer { get; set; }
        public int Issuee { get; set; }
        public string PaymentType { get; set; }
        public string Currency { get; set; }
        public InvoicePresentation Presentation { get{
            var items = InvoiceItem.AsQueryable();
            var itemsPres = items.Select(x => x.Presentation);

            return new InvoicePresentation{
                Id = Idinvoice,
                Time = TimeIssued,
                IssuerId = IssuerNavigation.Id,
                Issuer = IssuerNavigation.Presentation,
                RecipientId = IssueeNavigation.Id,
                Recipient = IssueeNavigation.Presentation,
                PaymentType = PaymentType,
                Currency = Currency,
                total = itemsPres.Aggregate(0.0M, (tot, x) => x.Product.price * (1.0M - (x.Discount??0.0M)) * (x.Quantity??0.0M) + tot),
                Items = itemsPres.ToArray()
            };
        }}

        public virtual Person IssueeNavigation { get; set; }
        public virtual Person IssuerNavigation { get; set; }
        public virtual ICollection<InvoiceItem> InvoiceItem { get; set; }
    }
}
