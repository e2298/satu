using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SATU;
using System.Text.Json;

namespace SATU.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        private readonly SATUContext _context;

        public InvoiceController(SATUContext context)
        {
            _context = context;
        }

        // GET: api/Invoice
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Invoice.InvoicePresentation>>> GetInvoice()
        {
            return await _context.Invoice
                .Include(x => x.InvoiceItem)
                    .ThenInclude(x => x.ProductNavigation)
                        .ThenInclude(x => x.CategoryNavigation)
                .Include(x => x.IssueeNavigation)
                    .ThenInclude(x => x.ActivityNavigation)
                .Include(x => x.IssuerNavigation)
                    .ThenInclude(x => x.ActivityNavigation)
                .Select(x => x.Presentation)
                .ToListAsync();
        }

        // GET: api/Invoice/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Invoice.InvoicePresentation>> GetInvoice(int id)
        {
            var invoice = await _context.Invoice
                .Include(x => x.InvoiceItem)
                    .ThenInclude(x => x.ProductNavigation)
                        .ThenInclude(x => x.CategoryNavigation)
                .Include(x => x.IssueeNavigation)
                    .ThenInclude(x => x.ActivityNavigation)
                .Include(x => x.IssuerNavigation)
                    .ThenInclude(x => x.ActivityNavigation)
                .Where(x => x.Idinvoice == id)
                .FirstOrDefaultAsync();

            if (invoice == null)
            {
                return NotFound();
            }

            return invoice.Presentation;
        }

        // POST: api/Invoice
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Invoice.InvoicePresentation>> PostInvoice(Invoice.InvoicePresentation invoice)
        {
            if(invoice.Id.HasValue){
                return BadRequest();
            }
            Invoice to_add;
            using (var trans = _context.Database.BeginTransaction()){
                try{
                    var issuer = await _context.PersonLatest.FindAsync(invoice.IssuerId);
                    var issuee = await _context.PersonLatest.FindAsync(invoice.RecipientId);

                    if(issuee == null || issuer == null)
                    {
                        return BadRequest();
                    } 

                    to_add = new Invoice(invoice, (int)issuer.Latest, (int)issuee.Latest);
                    _context.Invoice.Add(to_add);
                    await _context.SaveChangesAsync();


                    foreach(var item in invoice.Items){
                        to_add.InvoiceItem.Add(new InvoiceItem(item, (int)to_add.Idinvoice, (int)(await _context.ProductLatest.FindAsync(item.ProductId)).Latest));
                    }
                    await _context.SaveChangesAsync();

                    await (_context.Entry(to_add).Collection(x => x.InvoiceItem).LoadAsync());
                    await (_context.Entry(to_add).Reference(x => x.IssueeNavigation).LoadAsync());
                    await (_context.Entry(to_add).Reference(x => x.IssuerNavigation).LoadAsync());
                    to_add = await _context.Invoice
                        .Include(x => x.InvoiceItem)
                            .ThenInclude(x => x.ProductNavigation)
                                .ThenInclude(x => x.CategoryNavigation)
                        .Include(x => x.IssueeNavigation)
                            .ThenInclude(x => x.ActivityNavigation)
                        .Include(x => x.IssuerNavigation)
                            .ThenInclude(x => x.ActivityNavigation)
                        .Where(x => x.Idinvoice == to_add.Idinvoice)
                        .FirstAsync();


                    await trans.CommitAsync();
                }catch(Exception e){
                    Console.WriteLine(e);
                    await trans.RollbackAsync();
                    return BadRequest();
                }
            }
            return CreatedAtAction("GetInvoice", new { id = to_add.Idinvoice }, to_add.Presentation);
        }

        private bool InvoiceExists(int id)
        {
            return _context.Invoice.Any(e => e.Idinvoice == id);
        }
    }
}
