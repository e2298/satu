using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SATU;

namespace SATU.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActivityController : ControllerBase
    {
        private readonly SATUContext _context;

        public ActivityController(SATUContext context)
        {
            _context = context;
        }

        // GET: api/Activity
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Activity.ActivityPresentation>>> GetActivity()
        {
            return await _context.Activity.Where(e => e.Exists != 0).Select(e => e.Presentation).ToListAsync();
        }

        // GET: api/Activity/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Activity.ActivityPresentation>> GetActivity(int id)
        {
            var activity = await _context.Activity.Where(e => e.Exists != 0 && e.Dbid == id).FirstOrDefaultAsync();

            if (activity == null)
            {
                return NotFound();
            }

            return activity.Presentation;
        }

        // POST: api/Activity
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Activity.ActivityPresentation>> PostActivity(Activity.ActivityPresentation activity)
        {
            try{
                var n = new Activity(activity);
                _context.Activity.Add(n);
                await _context.SaveChangesAsync();
                await _context.Entry(n).ReloadAsync();
                activity = n.Presentation;
                return CreatedAtAction("GetActivity", new { id = activity.Id }, activity);
            }catch{
                return BadRequest();
            }


        }

        // DELETE: api/Activity/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Activity.ActivityPresentation>> DeleteActivity(int id)
        {
            var activity = await _context.Activity.FindAsync(id);
            if (activity == null)
            {
                return NotFound();
            }

            activity.Exists = 0;
            await _context.SaveChangesAsync();

            return activity.Presentation;
        }

        private bool ActivityExists(int id)
        {
            return _context.Activity.Any(e => e.Dbid == id);
        }
    }
}
