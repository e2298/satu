using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SATU;

namespace SATU.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly SATUContext _context;

        public PersonController(SATUContext context)
        {
            _context = context;
        }

        // GET: api/Person
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Person.PersonPresentation>>> GetPerson()
        {
            return await _context.PersonLatest.Include(e => e.LatestNavigation.ActivityNavigation)
                .Select(e => e.LatestNavigation.Presentation).
                ToListAsync();
        }

        // GET: api/Person/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Person.PersonPresentation>> GetPerson(string id)
        {
            var person = await _context.PersonLatest.Include(e => e.LatestNavigation.ActivityNavigation)
                .Where(e => e.Id == id)
                .Select(e => e.LatestNavigation.Presentation)
                .FirstOrDefaultAsync();

            if (person == null)
            {
                return NotFound();
            }

            return person;
        }

        // POST: api/Person
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Person.PersonPresentation>> PostPerson(Person.PersonPresentation person)
        {
            try{
                var per = new Person(person);
                _context.Person.Add(per);
                await _context.SaveChangesAsync();
                await _context.Entry(per).ReloadAsync();
                await _context.Entry(per).Reference(e => e.ActivityNavigation).LoadAsync();
                person = per.Presentation;
                return CreatedAtAction("GetPerson", new { id = person.Id }, person);
            }catch{
                return BadRequest();
            }
        }

        // DELETE: api/Person/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Person.PersonPresentation>> DeletePerson(string id)
        {
            var person = await _context.PersonLatest.FindAsync(id);
            if (person == null)
            {
                return NotFound();
            }
            await _context.Entry(person).Reference(e => e.LatestNavigation).LoadAsync();
            var res = person.LatestNavigation;
            await _context.Entry(res).Reference(e => e.ActivityNavigation).LoadAsync();

            _context.PersonLatest.Remove(person);
            await _context.SaveChangesAsync();

            return res.Presentation;
        }

        private bool PersonExists(int? id)
        {
            //TODO: WTF
            return _context.Person.Any(e => e.Dbid == id);
        }
    }
}
