using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SATU;
using System.Text.Json;

namespace SATU.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly SATUContext _context;

        public ProductController(SATUContext context)
        {
            _context = context;
        }

        // GET: api/Product
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Product.ProductPresentation>>> GetProduct()
        {
            return await _context.ProductLatest.Include(e => e.LatestNavigation.CategoryNavigation)
                .Select(e => e.LatestNavigation.Presentation)
                .ToListAsync();
        }

        // GET: api/Product/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Product.ProductPresentation>> GetProduct(int id)
        {
            var product = await _context.ProductLatest.Include(e => e.LatestNavigation.CategoryNavigation)
                .Where(e => e.Appid == id)
                .Select(e => e.LatestNavigation.Presentation)
                .SingleOrDefaultAsync();

            if (product == null)
            {
                return NotFound();
            }

            return product;
        }

        // POST: api/Product
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Product.ProductPresentation>> PostProduct(Product.ProductPresentation product)
        {
            try{
                var prod = new Product(product);
                _context.Product.Add(prod);
                await _context.SaveChangesAsync();
                await _context.Entry(prod).ReloadAsync();
                await _context.Entry(prod).Reference(e => e.CategoryNavigation).LoadAsync();
                return CreatedAtAction("GetProduct", new {id = prod.Appid}, prod.Presentation);
            }
            catch{
                return BadRequest();
            }
        }

        // DELETE: api/Product/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Product.ProductPresentation>> DeleteProduct(int id)
        {
            var product = await _context.ProductLatest.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            var ret = await _context.ProductLatest.Where(e => e.Appid == id).Select(e => e.LatestNavigation.Presentation).SingleOrDefaultAsync();

            _context.ProductLatest.Remove(product);
            await _context.SaveChangesAsync();

            return ret;
        }

        private bool ProductExists(int id)
        {
            return _context.Product.Any(e => e.Dbid == id);
        }
    }
}
